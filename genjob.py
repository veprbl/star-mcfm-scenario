#!/usr/bin/env python

import os
import shutil
import random

PDF_NAME = "CT10nlo.LHgrid"
PDF_NUM_MEMBERS = 52
APPLGRID_NUM_JOBS = 100

SQRT_S = 510

BASEPATH = os.path.dirname(os.path.abspath(__file__))

def mk_job_dir(dirname):
    os.mkdir(dirname)
    LINKS = {
        "br.sm1" : "MCFM-6.8/Bin/br.sm1",
        "br.sm2" : "MCFM-6.8/Bin/br.sm2",
        "dm_parameters.DAT" : "MCFM-6.8/Bin/dm_parameters.DAT",
        "hto_output.dat" : "MCFM-6.8/Bin/hto_output.dat",
        "process.DAT" : "MCFM-6.8/Bin/process.DAT",
        "fferr.dat" : "MCFM-6.8/QCDLoop/ff/fferr.dat",
        "ffperm5.dat" : "MCFM-6.8/QCDLoop/ff/ffperm5.dat",
        "ffwarn.dat" : "MCFM-6.8/QCDLoop/ff/ffwarn.dat",
        "PDFsets" : "share/lhapdf/PDFsets",
    }
    for name, dest in LINKS.items():
        os.symlink(
            os.path.join(BASEPATH, dest),
            os.path.join(dirname, name)
        )

def mk_inputfile(dirname, args, member):
    with open(os.path.join(BASEPATH, "MCFM-6.8/Bin/input.DAT"), "r") as ifp:
        lines = ifp.readlines()
    with open(os.path.join(dirname, "input.DAT"), "w") as ofp:
        for l in lines:
            if args.mode == 'applgrid' and l.find("[creategrid]") != -1:
                l = ".true.\t\t[creategrid]\n"
            if l.find("[writedat]") != -1:
                l = ".true.\t\t[writedat]\n"
            if l.find("[writetop]") != -1:
                l = ".false.\t\t[writetop]\n"
            if l.find("[writeroot]") != -1:
                l = ".false.\t\t[writeroot]\n"
            if l.find("[nproc]") != -1:
                l = "%i\t\t[nproc]\n" % args.proc
            if l.find("[part 'lord','real' or 'virt','tota']") != -1:
                l = "'tota'\t\t[part 'lord','real' or 'virt','tota']\n"
            if l.find("['runstring']") != -1:
                l = "''\t\t['runstring']\n"
            if l.find("[sqrts in GeV]") != -1:
                l = "%id0\t\t[sqrts in GeV]\n" % SQRT_S
            if l.find("[ncall1]") != -1:
                l = "1000000\t\t[ncall1]\n"
            if l.find("[ncall2]") != -1:
                l = "1000000\t\t[ncall2]\n"
            if l.find("[ij]") != -1:
                l = "%i\t\t[ij]\n" % random.randint(0, 2147483647)
            if l.find("[LHAPDF group]") != -1:
                l = "%s\t\t[LHAPDF group]\n" % PDF_NAME
            if l.find("[LHAPDF set]") != -1:
                l = "%i\t\t[LHAPDF set]\n" % member
            ofp.write(l);

class Job(object):
    def __init__(self, outdir, args, pdf_member):
        self._outdir = outdir
        self._args = args
        self._pdf_member = pdf_member

    def __str__(self):
        return "InitialDir = %s\nQueue\n" % self._outdir

    def prepare_outdir(self):
        mk_job_dir(self._outdir)
        mk_inputfile(self._outdir, self._args, self._pdf_member)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--job_name", type=str)
    parser.add_argument("--seed", type=int, default=None, help="pseudorandom generator seed")
    parser.add_argument("--proc", type=int, help="MCFM process: 1 for W+, 6 for W-")
    parser.add_argument("--mode", type=str, default='pdf_uncertainty', choices=['pdf_uncertainty', 'applgrid'])
    args = parser.parse_args()

    if args.seed is not None:
        random.seed(args.seed)

    jobs = []

    if args.mode == 'pdf_uncertainty':
        for member in range(PDF_NUM_MEMBERS+1):
            outdir = "%s_member%02i" % (args.job_name, member)
            jobs.append(Job(outdir, args, member))
    elif args.mode == 'applgrid':
        for i in range(APPLGRID_NUM_JOBS):
            outdir = "%s_%02i" % (args.job_name, i)
            jobs.append(Job(outdir, args, 0))

    for j in jobs:
        j.prepare_outdir()

    with open("%s.job" % args.job_name, "w") as jfp:
        jfp.write("""Universe = vanilla
Executable = {executable}
Notification = Never
Log = job.log
Output = out.log
Error = err.log
GetEnv = True
+Experiment = "star"

""".format(executable=os.path.join(BASEPATH, "MCFM-6.8/Bin/mcfm")) + "".join(map(str, jobs)))
