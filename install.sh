#!/bin/sh

INSTALLDIR=`pwd`
export PATH=${INSTALLDIR}/bin:${PATH}
export LD_LIBRARY_PATH=${INSTALLDIR}/lib:${LD_LIBRARY_PATH}

tar zxf lhapdf-5.9.1.tar.gz
pushd lhapdf-5.9.1
./configure --prefix=${INSTALLDIR} --disable-pyext || exit 1
make clean || exit 1
make install || exit 1
popd

WITH_APPLGRID=$(echo "$*" | egrep -- "--with-\<applgrid\>")
if [[ -n $WITH_APPLGRID ]]; then
        pushd mcfm-bridge-0.0.35
        ./configure --prefix=${INSTALLDIR} || exit 1
        make clean || exit 1
        make install || exit 1
        popd
fi

pushd MCFM-6.8
git checkout HEAD -- makefile Install || exit 1
git checkout HEAD -- Install || exit 1
sed -i "s#set LHAPDFLIB   = \$#export LHAPDFLIB=${INSTALLDIR}/lib#" Install
./Install
sed -i "s#PDFROUTINES = NATIVE#PDFROUTINES = LHAPDF#" makefile
sed -i "s#FFLAGS \t= -f#FFLAGS \t= -Wl,-rpath \$(LHAPDFLIB) -f#" makefile
rm obj/gridwrap.o
if [[ -n $WITH_APPLGRID ]]; then
        sed -i "/#LIBFLAGS += -ldhelas/a LIBFLAGS += \`${INSTALLDIR}/bin/mcfmbridge-config --ldflags\`" makefile
        rm -v src/User/gridwrap.f
        sed -i "/for c++ targets/ { n; s/^#//; n; s/^#// }" makefile
else
        git checkout HEAD -- src/User/gridwrap.f
fi
make || exit 1
popd

#bin/lhapdf-getdata CT10nlo.LHgrid
wget -O CT10nlo.LHgrid https://lhapdf.hepforge.org/downloads?f=pdfsets/5.9.1/CT10nlo.LHgrid
