#!/usr/bin/env python

import numpy as np

from genjob import PDF_NAME, PDF_NUM_MEMBERS

PDF_NAME = PDF_NAME.replace(".LHgrid", "")

DAT_PLOT_START_TAG = "HIST =   1"
DAT_PLOT_END_TAG = ""

class Hist(object):
    def __init__(self):
        self.xs = []
        self.ys = []
        self.dys = []


def read_dat(filepath, start_tag=DAT_PLOT_START_TAG, end_tag=DAT_PLOT_END_TAG):
    with open(filepath, "r") as fp:
            lines = fp.readlines()
    read_plot = False
    hs = []
    h = Hist()
    for l in lines:
        l = l.strip()
        if l == start_tag:
            read_plot = True
        elif l == end_tag:
            if read_plot:
                read_plot = False
                h.xs = np.array(h.xs)
                h.ys = np.array(h.ys)
                h.dys = np.array(h.dys)
                hs.append(h)
                h = Hist()
        elif read_plot:
            vs = l.split()
            if len(vs) == 3:
                try:
                    (x, y, dy) = map(float, vs)
                    h.xs.append(x)
                    h.ys.append(y)
                    h.dys.append(dy)
                except ValueError:
                    pass
    return hs


def master_formula(get_value_func, num_members):
    val_0 = get_value_func(0)
    dys_p_sq = np.zeros(len(val_0.xs))
    dys_n_sq = np.zeros(len(val_0.xs))
    for member in range(1, num_members, 2):
        val_p = get_value_func(member)
        val_n = get_value_func(member + 1)
        assert (val_0.xs == val_p.xs).all()
        assert (val_0.xs == val_n.xs).all()
        dys_p_sq += np.max([val_p.ys - val_0.ys, val_n.ys - val_0.ys, np.zeros(len(val_0.ys))], 0) ** 2
        dys_n_sq += np.min([val_p.ys - val_0.ys, val_n.ys - val_0.ys, np.zeros(len(val_0.ys))], 0) ** 2
    val_0.dys_p = np.sqrt(dys_p_sq)
    val_0.dys_n = np.sqrt(dys_n_sq)
    return val_0


def one(xs):
    assert len(xs) == 1
    return xs[0]


def W_sec_pdf_uncertainty(member_file_fmt, num_members):
    def get_W_sec_value(member):
        return one(read_dat(member_file_fmt % member))
    return master_formula(get_W_sec_value, num_members)


def W_sec_pdf_uncertainty_mcfm_member_loop(member_file_fmt, num_members):
    """
    Load per PDF member cross sections from _error file produced by MCFM with PDFset=-1 option.

    This is of interest because MCFM seems to produce different cross sections than when run separately
    for different PDF set members.
    """
    def get_W_sec_value(member):
        if member == 0:
            h = one(read_dat(member_file_fmt % "", "TITLE TOP SIZE=3\"W rapidity", "PLOT"))
            # Workaround MCFM skipping low statistics bins
            h.xs = h.xs[4:-4]
            h.ys = h.ys[4:-4]
            h.dys = h.dys[4:-4]
            return h
        else:
            return read_dat(member_file_fmt % "_error", "SET ORDER X Y DY", "PLOT")[member-1]
    return master_formula(get_W_sec_value, num_members)


def WpWm_relation_pdf_uncertainty(Wp_member_file_fmt, Wm_member_file_fmt, num_members):
    def get_W_relation(member):
        val_Wp = one(read_dat(Wp_member_file_fmt % member))
        val_Wm = one(read_dat(Wm_member_file_fmt % member))
        h = Hist()
        h.xs = val_Wp.xs
        h.ys = val_Wp.ys / val_Wm.ys
        return h
    return master_formula(get_W_relation, num_members)


if __name__ == '__main__':
    order = "tota"
    val_p = W_sec_pdf_uncertainty("wplus_member%%02i/W_only_%s_%s_80__80__.dat" % (order, PDF_NAME), PDF_NUM_MEMBERS)
    val_n = W_sec_pdf_uncertainty("wminus_member%%02i/W_only_%s_%s_80__80__.dat" % (order, PDF_NAME), PDF_NUM_MEMBERS)
    #val_mcfm = W_sec_pdf_uncertainty_mcfm_member_loop("wminus_pdferr/W_only_%s_%s_80__80__%%s.top" % (order, PDF_NAME), PDF_NUM_MEMBERS)
    val = WpWm_relation_pdf_uncertainty(
            "wplus_member%%02i/W_only_%s_%s_80__80__.dat" % (order, PDF_NAME),
            "wminus_member%%02i/W_only_%s_%s_80__80__.dat" % (order, PDF_NAME),
            PDF_NUM_MEMBERS)
    for row in zip(val.xs, val.ys, val.dys_p, val.dys_n):
        print "%.1f\t%.2f\t%.2f\t%.2f" % row
